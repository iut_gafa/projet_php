<?php
//On se connect à la base
$connexion = new mysqli('localhost', 'phpAccess', 'phpAccess', 'offres_stages');
if ($connexion->connect_error) {
	die('Connexion à la BDD échoué (' . $connexion->connect_errno . ') ' . $connexion->connect_error);
	$connexion->close();
}

//On lance la requête
$res = $connexion->query("select id_offre, titre, raison_sociale
	from offre O join entreprise E on O.id_user = E.id_user;");

if (!$res) {
	die('Echec d\'éxécution de la requête' . $connexion->errno . ' ' . $connexion->error);
}

$tab = $res->fetch_all(MYSQLI_ASSOC);
$connexion->close();
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="style.css">
		<title></title>
	</head>

	<body>
		<div class="list">
			<table>
				<tr>
					<th>Stage</th>
					<th>Entreprise</th>
				</tr>
				<?php
				foreach ($tab as $row) {
					echo("<tr>
						<td>"
							.'<a title="lien vers loffre" href="offre.php?id='.$row['id_offre'].'">'.$row['titre']."</a></td>
						<td>".$row['raison_sociale']."</td>
					</tr>");
				}
				?>
			</table>
		</div>
	</body>
</html>
