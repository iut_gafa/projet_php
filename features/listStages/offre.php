<?php
if (!isset($_GET['id'])) {
	header('Location: listStage.php');
	exit();
}
$idOffre=$_GET['id'];
//On se connect à la base
$connexion = new mysqli('localhost', 'phpAccess', 'phpAccess', 'offres_stages');
if ($connexion->connect_error) {
	die('Connexion à la BDD échoué (' . $connexion->connect_errno . ') ' . $connexion->connect_error);
	$connexion->close();
}

//On lance la requête
$res = $connexion->query("select *
	from offre O join entreprise E on O.id_user = E.id_user
	where id_offre=$idOffre;");

if (!$res) {
	die('Echec d\'éxécution de la requête' . $connexion->errno . ' ' . $connexion->error);
}
$tab = $res->fetch_all(MYSQLI_ASSOC);

//On se déconnecte
$connexion->close();
?>

<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		<?php
		foreach ($tab as $row) {
			echo("<h1>".$row['titre']."</h1>");
			$index=array("raison_sociale", "date_deb", "duree", "mission", "contact", "commentairec", "piece_jointe");
			foreach ($index as $currentIndex) {
				echo("<p> $currentIndex : ".$row[$currentIndex]."</p>");
			}
		}
		?>
	</body>
</html>
