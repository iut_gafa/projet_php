Création d'un site qui gère des offres de stage.

4 utilisateur
 |
 |- visiteur observe.
 |- Membre peut liker les offres -> peuvent créer un compte comme ça.
 | \
 |  |- S'inscrire.
 |  |- Se connecter / Déconnecter.
 |  |- Manager son compte.
 |
 |- Entreprise = membre qui peut manager les offres de stage -> l'inscription est renvoyer à l'admin pour valider les inscription des entreprises.
 | \
 |  |- pareil que visiteur.
 |  |- s'inscrire (nom, prénom, mail, mdp, raison sociale, tel).
 |  |- Ajouter / Supprimer une annonce (titre, énoncé, date début, mission, contact champ libre, pièce jointe facultatif, complément d'info facultatif).
 |
 |- Admin = compte UNIQUE crée lors de l'installation du site -> peut valider le compte entreprise mais aussi modifier et supprimer les comptes.
 | \
 |  |- Idem que les deux autres.
 |  |- Peut liker et disliker.
 |  |- Peut commenter et modifier le commentaire.
 |  |- Gérer les comptes (entreprise et utilisateurs, création / modification / suppression).

Il faut créer un installateur.
En plus du site il faut :
une documentation -> mkdoc.org
Description du site
Diagramme de classe de la BDD
	et des composant
La description de l'architecture -> modèle MVC

La revue de projet
Préparer un scénario de test avec installation -> création d'un compte entreprise -> création d'un compte utilisateur -> obs
La note :
- Test de performance
- Doc tech
- Les pts bonus

list :
- page offres (afficher tout les champs et trier par like).
- page admin (commenter, supprimer, ajouter et supprimer les offres).
- page inscription utilisateur.
- page inscription entreprise.

Pour la gestion des offres il manque tous ce qui est de la gestion des propriétaire des offres
