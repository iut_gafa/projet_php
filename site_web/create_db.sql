-- Table User.
create table users (
	id_user int auto_increment not null,
	nom VARCHAR(50) not null,
	mail varchar(50) not null,
	mdp TEXT not null,
	tel char(10),
	statut enum ('admin', 'membre', 'entreprise'),
	primary key (id_user),
	CONSTRAINT CHECK (tel like '0[0-9]{9}')
);

CREATE TABLE entreprise (
	id_user int not null,
	raison_sociale varchar(50) not null,
	activated boolean,
	PRIMARY KEY (id_user),
	CONSTRAINT FOREIGN KEY (id_user) REFERENCES users(id_user)
);

CREATE TABLE membre (
	id_user INT NOT NULL,
	prenom varchar(50) not null,
	date_nais DATE NOT NULL,
	PRIMARY KEY (id_user),
	CONSTRAINT FOREIGN KEY (id_user) REFERENCES users(id_user)
);

CREATE TABLE offre
(
    id_offre INT auto_increment,
	id_user int not null,
    titre VARCHAR(200) NOT NULL,
    date_deb DATE NOT NULL,
    date_fin DATE NOT NULL,
    date_crea TIMESTAMP NOT NULL,
    contact TEXT NOT NULL,
    piece_jointe TEXT DEFAULT NULL,
    mission TEXT DEFAULT NULL,
    commentaire TEXT DEFAULT NULL,
    -- Contraintes :
		PRIMARY KEY (id_offre, id_user),
		CONSTRAINT FOREIGN KEY (id_user) REFERENCES entreprise(id_user)
);

CREATE TABLE liker (
	id_user INT not null,
	id_offre INT not null,
	PRIMARY KEY(id_user, id_offre),
	constraint FOREIGN KEY (id_user) REFERENCES users(id_user),
	constraint FOREIGN KEY (id_offre) REFERENCES offre(id_offre)
);
