# Home

## like( integer )
> Like ou Dé-like une offre.<br/>
> ```$id_offre``` : représente l'offre à liker ou à dé-liker.
```php
public function like($id_offre)
```

# Log

## logout ( )
> Déconnecte l'utilisateur courant.
```php
public function logout()
```

# Offers

## listAll( bool )
> Charge les vues pour afficher toutes les offres rangé par dates ou par nombre de j'aime.
```php
public function listAll(bool $ordered_by_like)
```
