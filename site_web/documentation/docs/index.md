# Accueil

## Introduction
#### Bienvenue sur la v.1 de la documentation du groupe n°24 pour le projet de programmation web 2019 !
#### Dans cette documentation, vous trouverez les informations concernant l'état actuel du projet.
Attention, dans cette version, la documentation n'est pas complète.

## Description

#### L'objectif de ce projet est un site web de gestion d’offres de stages.
#### Il y a donc 4 types d'utilisateurs :

| Actions | 0. Visiteur | 1. Étudiant | 2. Entreprise | 3. Admin|
|--------------------------- | - | - | - | - |
| Voir les offres            | O | O | O | O |
| Liker/Dé-liker             |   | O | O | O |
| Ajouter une offre          |   |   | O |   |
| Modifier/Supprimer offres  |   |   | O | O |
| Commenter une offre        |   |   |   | O |

## Installation

* `unzip` l'archive du siteweb.
* Modifier le fichier ```./application/config/config.php``` si l'adresse du site n'est pas la même.
* Lancer le contrôleur `Install/create`, pour créer les tables de la base de données.
* Lancer le contrôleur `Install/load`, pour remplir les tables de la base de données.

## Architecture du site

### 1. Les Modèles :

* [MListStage](modeles.md#MListStage) : Connaît les requêtes des offres avec la base de données.

* [MSign](modeles.md#MSign) : Connaît les connexions et les inscriptions.

* [MUsers](modeles.md#MUsers) : Connaît la navigation des utilisateurs sur le site.

### 2. Les Vues :

* [Menu](vues.md#Menu) : Affiches la bar de navigation du site.

* [VLisStage](vues.md#VLisStage) : Affiche toutes les offres de stages.

* [VParticularOffer](vues.md#VParticularOffer) : Affiche une offre en particulier.

### 3. Les Contrôleurs :

* [Home](offres.md#Home) : Gère la page d'accueil du site.

* [Log](offres.md#Log) : Gère les nouvelles connexions et inscriptions.

* [Offers](offres.md#Offers) : Gère les différents affichages des offres.

## Diagramme des Classes de la Base de Données

![img-dingus](./img/php_diag_v2.png)
