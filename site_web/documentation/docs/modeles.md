#MListStage

## getAll( bool )
> Renvoie toutes les offres rangé par dates ou par nombre de j'aime.
```php
public function getAll (bool $ordered_by_like)
```

## insertNewOffer( array )
> Ajoute une offre à la base de données. <br/>
> ```$input``` : représente les données de l'offre.
```php
public function insertNewOffer(&$input) : bool
```

## updateOffer( bool )
> Modifie une offre de la base de données. <br/>
> ```$input``` : représente les nouvelles données de l'offre.
```php
public function updateOffer(&$input) : bool
```


#MSign

## add_member( string, string, string, string, string, string )
> Ajoute un nouveau membre à la base de données.
> Et renvoie si l'insertion s'est bien passé.
```php
public function add_member(string $date, string $nom, string $prenom, string $email, string $mdp, string $tel):bool
```

#MUsers
## listUsers( string )
> Renvoie la liste de tout les membres ou de toutes les entreprises de la base de données.<br/>
> ```$userType``` : représente le type d'utilisateurs ('membre' ou 'entreprise').
```php
public function listUsers(string $userType)
```
