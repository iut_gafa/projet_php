<?php
class Entreprise extends CI_Controller {
	public function index() {
		$this->load->helper('url');

		redirect('Home');
	}

	public function createOffer() {
		if ($this->checkUserRights('entreprise', 0)) {
			$this->load->view('scripts/connection');
			$this->load->view('headers/menu');
			$this->load->view('VCreateOffer');
		} else {
			redirect('Home');
		}
	}

	//Est chargé du stockage des changement dans la bdd (insertion ou modification).
	public function validateStockIntoDB() {
		$data=array();
		$this->load->helper('url');
		$data['formInput'] = isset($_POST) ? $_POST : array();

		//On verifie que l'on vient du formulare en ayant clicker sur valider
		//Et que l'utilisateur qui l'a fait à les droits.
	if (isset($_POST['valider']) && (
		($this->checkUserRights('entreprise', (int)($data['formInput']['id_user']))) ||
			($this->checkUserRights('entreprise', 0) && $data['formInput']['id_user'] == '') ||
			($this->checkUserRights('admin', 0) && $data['formInput']['id_user'] != '')
		)
	) {

			//On place les donnée du formulaire dans un tableau
			$this->load->model('MListStage');

			//On tente d'insérer les données dans la base sinon on retourne sur le formulaire.
			//$data['formInput']['id_user'] ?? $this->session->user_info->id_user;
			$data['formInput']['id_user'] = $data['formInput']['id_user'] == '' ? $this->session->user_info->id_user : $data['formInput']['id_user'];

			//On test si l'on tente une insertion ou une modification.
			//Si on fait une insertion
			if (!(isset($data['formInput']['id_offre']) && $data['formInput']['id_offre'] !== '')) {
				$validated = $this->MListStage->insertNewOffer($data['formInput']);
			} else {
			//En cas de modification.
				$validated = $this->MListStage->updateOffer($data['formInput']);
			}

			if ($validated) {
				redirect('Home');
			} else {
				$this->load->view('scripts/connection');
				$this->load->view('headers/menu');
				$this->load->view('VCreateOffer', $data);
			}
		} else {
			redirect('Home');
		}
	}

	//Affiche le formulaire d'édition. -> il manque la vérification du propriétaire
	public function editOffer($id_offre) {
		//On demande les données au model.
		$this->load->model('MListStage');
		$query = $this->MListStage->getOffersById($id_offre);

		//On vérifie que l'on a bien un résultat.
		//Et on vérifie que l'on a les droits d'édition.
	if ($query[0] != array() && ($this->checkUserRights('admin', 0) || $this->checkUserRights('entreprise', $query[0]->id_user))) {

			$data['formInput'] = array(
				'id_user' => $query[0]->id_user,
				'id_offre' => $query[0]->id_offre,
				'titre' => $query[0]->titre,
				'date_deb' => $query[0]->date_deb,
				'date_fin' => $query[0]->date_fin,
				'contact' => $query[0]->contact,
				'mission' => $query[0]->mission,
				'commentairec' => $query[0]->commentaire
			);

			$this->load->helper('url');
			$this->load->view('scripts/connection');
			$this->load->view('headers/menu');
			$this->load->view('VCreateOffer', $data);
		} else {
			redirect('Home');
		}
	}

	//Supprime l'offre d'une entreprise -> il manque la vérification du propriétaire
	public function deleteOffer($id_offre) {
		$this->load->helper('url');
		$this->load->model('MListStage');

		//On fait une requête pour récupérer l'id du propriétaire de l'offre.
		$query = $this->MListStage->getOffersById($id_offre);
		if (!($this->checkUserRights('admin', 0) || $this->checkUserRights('entreprise', $query[0]->id_user))) {
			redirect('Home');
		}

		$this->MListStage->deleteOfferById($id_offre);

		redirect('Home');
	}

	public function viewMyAccount() {
		$this->load->helper('url');
		$this->load->library('session');
		//On vérifie qu'une session est ouverte.
		if (isset($this->session->user_info)) {
			$id = (int)($this->session->user_info->id_user);
			redirect('Admin/userDetails/'.$this->session->user_info->statut.'/'.$id);
		} else {
			redirect('Home');
		}
	}

	public function checkUserRights(string $searchType, int $id) : bool {
		//On load la librairie qui gère les sessions
		$this->load->library('session');
		//On vérifie qu'une session soit ouverte
		if (isset($this->session->user_info)) {
			//On vérifie que les status correspondent
			if ($searchType == 'admin' || $searchType == 'membre' || $searchType == 'entreprise') {
				$retour = $searchType == $this->session->user_info->statut;
			} else {
				$retour = false;
			}

			//On demande une vérification de l'id elle est faite
			if ($id != 0) {
				$retour = $retour && $id == $this->session->user_info->id_user;
			}
		} else {
			//Si aucun des status en paramêtre est correcte on renvoie false.
			$retour = false;
		}
		return $retour;
	}

	public function redirectIfNotAllowed(string $searchType, int $id) {
		$this->checkUserRights($searchType, $id);
	}
}
