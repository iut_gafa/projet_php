<?php
	include_once (dirname(__FILE__) . "/Offers.php");

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Home extends Offers
	{

		public function index(string $sort = "date")
		{
			$this->load->helper('url');
			$this->load->library('session');
			$this->session->set_flashdata('page', 'false/Home');
			$this->load->view('welcome_message');
			$this->listAll(!($sort=="date"));
		}

		public function like($id_offre)
		{
			parent::like($id_offre);
			$this->index();
		}
	}
