<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Log extends CI_Controller
	{
		public function trylog(string $is_view = "true", ...$pages)
		{
			$page = '';
			foreach ($pages as $n)
			{
				$page .= $n . '/';
			}
			$page = substr($page, 0, -1);

			$this->load->library('session');
			$this->load->helper(array('form', 'url'));
			// Est ce que on vient bien de notre formulaire :
			if ($this->input->post('action')=="SignIn")
			{
				$this->load->model('mSign');

				$log_email = $this->input->post('log_email');
				$log_psw = $this->input->post('psw');

				$this->_set_modal_default("signinModal");


				// Si l'utilisateur n'est pas dans la base :
				if (!$this->mSign->check_user($log_email))
				{
					$this->session->set_flashdata('is_mail_valid_si', 'is-invalid');
					($is_view == "true")?$this->load->view($page):redirect($page);

				}
				else // L'utilisateur existe :
				{
					$this->session->set_flashdata('is_mail_valid_si', 'is-valid');
					// Le mot de passe est bon :
					if ($this->mSign->check_psw($log_email, $log_psw))
					{
						$this->_log($log_email, $log_psw);
						($is_view == "true")?$this->load->view($page):redirect($page);
					}
					else // Mauvais mot de passe :
					{
						$this->session->set_flashdata('is_psw_valid_si', 'is-invalid');
						($is_view == "true")?$this->load->view($page):redirect($page);
					}
				}
			}
			else // Si on vient pas de notre formulaire on l'envoit à l'index :
			{
				redirect('Home');
			}
		}

		private function _set_modal_default(string $modal)
		{
			$this->session->set_flashdata('modal_settings', '
			<script type="text/javascript">
				$(window).on(\'load\',function(){ $(\'#' . $modal . '\').modal(\'show\'); });
			</script>');
		}

		public function signup(string $is_view = "true", ...$pages)
		{
			$this->load->model('mSign');
			$page = '';
			foreach ($pages as $n)
			{
				$page .= $n . '/';
			}

			$page = substr($page, 0, -1);

			$this->load->helper(array('form', 'url'));
			$this->load->library('session');

			// Est ce que on vient bien de notre formulaire :
			if ($this->input->post('action')=="SignUp")
			{
				$this->load->model('mSign');

				$sup_email = $this->input->post('sup_email');
				$tel = $this->input->post('tel');
				$sup_pswa = $this->input->post('sup_pswa');
				$sup_pswb = $this->input->post('sup_pswb');
				$is_company = $this->input->post('is_company');
				$sup_name = $this->input->post('sup_name');
				$sup_other_name = $this->input->post(($is_company == 'on')?'soc_reas':'first_name');
				$birth_day = $this->input->post('birth_day');

				$this->_set_modal_default("signupModal");

				// Si l'email n'existe pas :
				if (!$this->mSign->check_user($sup_email))
				{
					$this->session->set_flashdata('is_email_valid_su', 'is-valid');

					// Si le tel est bon :
					if (preg_match("#^0[0-9]{9}$#", $tel))
					{
						$this->session->set_flashdata('is_tel_valid_su', 'is-valid');

						// Si les mots de passe correspondent :
						if ($sup_pswa == $sup_pswb)
						{
							$this->session->set_flashdata('is_psw_valid_su', 'is-valid');
							if ($is_company == 'on')
							{
								// Success
								if($this->mSign->add_company($sup_name, $sup_other_name, $sup_email, $sup_pswa, $tel))
								{
									$this->session->set_flashdata('success_company', 'yes');
									unset($_SESSION['modal_settings']);
									$this->_log($sup_email, $sup_pswa);
									($is_view == "true")?$this->load->view($page):redirect($page);
									$this->load->view('headers/success_su');
								}
								/*
								else
								{
									$this->_set_modal_default("echecModal");
									($is_view == "true")?$this->load->view($page):redirect($page);
								} */
							}
							else
							{
								unset($_SESSION['modal_settings']);
								// Success
								if($this->mSign->add_member($birth_day, $sup_name, $sup_other_name, $sup_email, $sup_pswa, $tel))
								{
									$this->session->set_flashdata('success_company', 'yes');
									$this->_log($sup_email, $sup_pswa);
									($is_view == "true")?$this->load->view($page):redirect($page);
									$this->load->view('headers/success_su');
								}
								/*
								else
								{
									$this->_set_modal_default("echecModal");
									($is_view == "true")?$this->load->view($page):redirect($page);
								} */
							}
						}
						else
						{
							$this->session->set_flashdata('is_psw_valid_su', 'is-invalid');
							($is_view == "true")?$this->load->view($page):redirect($page);
						}
					}
					else
					{
						$this->session->set_flashdata('is_tel_valid_su', 'is-invalid');
						($is_view == "true")?$this->load->view($page):redirect($page);
					}
				}
				else
				{
					$this->session->set_flashdata('is_email_valid_su', 'is-invalid');
					($is_view == "true")?$this->load->view($page):redirect($page);
				}
			}
			else // Si on vient pas de notre formulaire on l'envoit à l'index :
			{
				redirect('Home');
			}

		}

		private function _log(string $email, string $psw)
		{
			unset($_SESSION['modal_settings']);
			$this->load->library('session');
			$user_info = $this->mSign->getuser($email, $psw);
			$this->session->user_info = $user_info;
		}

		private function _unset_verif()
		{
			unset($_SESSION['is_email_valid_su']);
			unset($_SESSION['is_tel_valid_su']);
			unset($_SESSION['is_pswa_valid_su']);
			unset($_SESSION['is_pswb_valid_su']);
		}

		public function logout()
		{
			$this->load->helper('url');
			$this->load->library('session');
			unset($_SESSION['user_info']);
			redirect('Home');
		}

		public function enable_company_account(string $id_comp, string $token)
		{
			$this->load->helper('url');
			$this->load->library('session');
			$this->session->set_flashdata('page', "false/Log/enable_company_account/$id_comp/$token");
			$this->load->view("active_account");
			$this->session->set_flashdata('hide_signin_button', 'yes');
			$this->session->set_flashdata('anti_closing', 'yes');

			// Si il est co ?
			if (isset($this->session->user_info))
		    {
				if ($this->session->user_info->statut == "admin")
			    {
					$this->load->model('mSign');
					if ($token == $this->mSign->hash_token($id_comp))
				    {
						$this->mSign->activate_account($id_comp);
						$this->load->view("headers/success_acti");
				    }
				    else
				    {
						redirect('Home');
				    }
			    }
			    else
			    {
					redirect('Home');
			    }
		    }
		    else // Si il est pas co
		    {
				$this->_set_modal_default("signinModal");
		    }
			$this->load->view("headers/menu");
		}

		public function checkUserRights(string $searchType, int $id) : bool {
			//On load la librairie qui gère les sessions
			$this->load->library('session');
			//On vérifie qu'une session soit ouverte
			if (isset($this->session->user_info)) {
				//On vérifie que les status correspondent
				if ($searchType == 'admin' || $searchType == 'membre' || $searchType == 'entreprise') {
					$retour = $searchType == $this->session->user_info->statut;
				} else {
					$retour = false;
				}

				//On demande une vérification de l'id elle est faite
				if ($id != 0) {
					$retour = $retour && $id == $this->session->user_info->id_user;
				}
			} else {
				//Si aucun des status en paramêtre est correcte on renvoie false.
				$retour = false;
			}
			return $retour;
		}

		public function redirectIfNotAllowed(string $searchType, int $id) {
			$this->checkUserRights($searchType, $id);
		}
	}
