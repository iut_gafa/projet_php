<?php
class Main extends CI_Controller {
	public function index() {
		$this->load->helper('url');
		$this->listAll();
	}

	public function listAll() {
		$data = array();
		$this->load->model('MListStage');
		$data['listStage'] = $this->MListStage->getAll();
		$this->load->view('VListStage', $data);
	}

	public function offersDetails($id) {
		$this->load->helper('url');

		$data = array();
		$this->load->model('MListStage');
		$data['stageInfo'] = $this->MListStage->getOffersById($id);

		$this->load->view('VParticularOffer', $data);
	}

}
