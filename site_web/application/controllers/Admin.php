<?php
include_once (dirname(__FILE__) . "/Offers.php");
class Admin extends Offers {
	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->helper('url');
		$this->redirectIfNotAllowed('admin', 0);

		$data = array();
		$this->load->model('MListStage');
		$data['listStage'] = $this->MListStage->getAll(false);

		$this->load->view('scripts/connection');
		$this->load->view('headers/menu');
		$this->load->view('VListStage', $data);
	}

	public function displayUsers(string $userType) {
		$this->load->helper('url');
		//Il faut vérifier que celui qui est ici ai bien les droits
		$this->redirectIfNotAllowed('admin', 0);
		//On redirect si l'option ne correspond pas.
		$this->redirectIfWrongUserType($userType);

		$data = array();
		$this->load->model('MUsers');
		$data['listUsers'] = $this->MUsers->listUsers($userType);
		$data['userType'] = $userType;

		$this->load->view('scripts/connection');
		$this->load->view('headers/menu');
		$this->load->view('VUsersList', $data);
	}

	public function userDetails(string $userType, int $id) {
		$this->load->helper('url');
		//Vérifions que les paramêtres sont corrects
		$this->redirectIfWrongUserType($userType);
		//Il faut vérifier les droits.
		if (!($this->checkUserRights($userType, $id) || $this->checkUserRights('admin', 0))) {
			redirect('Home');
		}

		$this->load->model('MUsers');
		$data['userData'] = $this->MUsers->userById($userType, $id);
		//On vérie si l'id existe dans la base
		if($data['userData'] != array()) {
			$data['userType'] = $userType;
			$this->load->view('scripts/connection');
			$this->load->view('headers/menu');
			$this->load->view('VUserDetails', $data);
		} else {
			redirect('Home');
		}
	}

	private function redirectIfWrongUserType(string $userType) {
		//On vérifie que userType est "membre" ou "entreprise".
		if (!($userType == 'entreprise' || $userType == 'membre')) {
			redirect('Home');
		}
	}

	public function deleteUser(int $id, string $userType) {
		if ($this->checkUserRights('entreprise', $id) || $this->checkUserRights('membre', $id) || $this->checkUserRights('admin', 0)) {
			$this->load->model('MUsers');
			$this->MUsers->deleteUser($userType, $id);

			if ($this->checkUserRights('admin', 0)) {
				redirect('Admin/displayUsers/membre');
			} else {
				redirect('Log/logout');
			}
		} else {
			redirect('Home');
		}
	}


	public function checkUserRights(string $searchType, int $id) : bool {
		//On load la librairie qui gère les sessions
		$this->load->library('session');
		//On vérifie qu'une session soit ouverte
		if (isset($this->session->user_info)) {
			//On vérifie que les status correspondent
			if ($searchType == 'admin' || $searchType == 'membre' || $searchType == 'entreprise') {
				$retour = $searchType == $this->session->user_info->statut;
			} else {
				$retour = false;
			}

			//On demande une vérification de l'id elle est faite
			if ($id != 0) {
				$retour = $retour && $id == $this->session->user_info->id_user;
			}
		} else {
			//Si aucun des status en paramêtre est correcte on renvoie false.
			$retour = false;
		}
		return $retour;
	}

	public function redirectIfNotAllowed(string $searchType, int $id) {
		if (!$this->checkUserRights($searchType, $id)) {
			redirect('Home');
		}
	}

}
