<?php
class Offers extends CI_Controller {
	public function index() {
		$this->load->helper('url');
		$this->listAll();
	}

	public function listAll(bool $ordered_by_like) {
		$data = array();
		$this->load->model('MListStage');
		$data['listStage'] = $this->MListStage->getAll($ordered_by_like);
		$data['sort_like'] = $ordered_by_like;

		$this->load->view('scripts/connection');
		$this->load->view('headers/menu');
		$this->load->view('VListStage', $data);
	}

	public function offersDetails($id) {
		$this->load->helper('url');

		//Demandons au model les information sur le stage.
		$data = array();
		$this->load->model('MListStage');
		$data['stageInfo'] = $this->MListStage->getOffersById($id);

		//Vérifions que nous avons un résultat
		if ($data['stageInfo'] != array()) {
			$data['userGotRights'] = ($this->checkUserRights('admin', 0) || $this->checkUserRights('entreprise', $data['stageInfo'][0]->id_user));

			$this->load->view('scripts/connection');
			$this->load->view('headers/menu');
			$this->load->view('VParticularOffer', $data);
		} else {
			redirect('Home');
		}
	}

	public function like($id_offre)
	{
		$this->load->library('session');
		$this->load->model('MListStage');
		// Si c'est deja like on dé-like :
		if ($this->MListStage->already_liked($this->session->user_info->id_user, $id_offre))
		{
			$this->MListStage->unlike($this->session->user_info->id_user, $id_offre);
		}
		else // Sinon on like
		{
			$this->MListStage->like($this->session->user_info->id_user, $id_offre);
		}
	}

	public function checkUserRights(string $searchType, int $id) : bool {
		//On load la librairie qui gère les sessions
		$this->load->library('session');
		//On vérifie qu'une session soit ouverte
		if (isset($this->session->user_info)) {
			//On vérifie que les status correspondent
			if ($searchType == 'admin' || $searchType == 'membre' || $searchType == 'entreprise') {
				$retour = $searchType == $this->session->user_info->statut;
			} else {
				$retour = false;
			}

			//On demande une vérification de l'id elle est faite
			if ($id != 0) {
				$retour = $retour && $id == $this->session->user_info->id_user;
			}
		} else {
			//Si aucun des status en paramêtre est correcte on renvoie false.
			$retour = false;
		}
		return $retour;
	}

	public function redirectIfNotAllowed(string $searchType, int $id) {
		$this->checkUserRights($searchType, $id);
	}

}
