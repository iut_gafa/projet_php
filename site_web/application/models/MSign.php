<?php

    class MSign extends CI_Model
    {
        function __construct()
        {
            // Call the Model constructor
            parent::__construct();
            $this->load->database();
        }

        public function getall()
        {
            $query = $this->db->get('users');
            //On test si elle est vide ou non.
            if ($query->num_rows() > 0)
            {
                return $query->result();
            }
            else
            {
                return array();
            }
        }

        public function check_user(string $log_mail):bool
        {
            return $this->db->from('users')->where('mail', $log_mail)->count_all_results();
        }

        public function add_member(string $date, string $nom, string $prenom, string $email, string $mdp, string $tel):bool
        {
            $this->db->query("INSERT INTO users VALUES (DEFAULT, '$nom', '$email', '" . $this->_hash_psw($mdp) . "', '$tel', 'membre');");
            $this->db->query("INSERT INTO membre VALUES ((SELECT id_user FROM users WHERE mail = '$email' LIMIT 1), '$prenom', '$date');");
            return true;
        }

        public function add_company(string $nom, string $social_reason, string $email, string $mdp, string $tel):bool
        {
            $this->db->query("INSERT INTO users VALUES (DEFAULT, '$nom', '$email', '" . $this->_hash_psw($mdp) . "', '$tel', 'entreprise');");
            $this->db->query("INSERT INTO entreprise VALUES ((SELECT id_user FROM users WHERE mail = '$email' LIMIT 1), '$social_reason', DEFAULT);");
            $this->_send_email($email);
            return true;
        }

        public function check_psw(string $log_mail, string $log_psw):bool
        {
            return $this->db->select('mdp')->from('users')->where('mail', $log_mail)->where('mdp', $this->_hash_psw($log_psw))->count_all_results();
        }

        private function _hash_psw(string $psw):string
        {
            return sha1(sha1($psw) . sha1("00Faee."));
        }

        public function hash_token(string $str):string
        {
            return sha1(sha1($str) . sha1(".0eeaF0."));
        }

        private function _send_email(string $email_company)
        {
            $query = $this->db->query("SELECT * FROM users U INNER JOIN entreprise E ON U.id_user = E.id_user WHERE mail ='$email_company' LIMIT 1;");

            if (!empty($query->row(0)))
            {
                $res = $query->row(0);
                $link = anchor("Log/enable_company_account/$res->id_user/". $this->hash_token($res->id_user));

                $message = "<html><body>" .
                    "INFORMATION :<br/>" .
                    "name : $res->nom<br/>" .
                    "social_reason : $res->raison_sociale<br/>" .
                    "email : $res->mail<br/>" .
                    "tel : $res->tel<br/>" .
                    "<br/>" .
                    "<br/>" .
                    "To activate this account : <br/>" .
                    "$link" .
                    "</body></html>";

                mail('gaetan.luitot@etu.univ-amu.fr', "Demande Acceptation Entreprise $res->nom", $message, "Content-Type: text/html; charset=UTF-8\r\n");
            }
            else
            {
                // Throw ...
                echo "ERREUR";
                echo $query->row(0);
            }

        }

        public function getuser(string $log_mail, string $psw)
        {
            $query = $this->db->get_where('users', ['mail' => $log_mail, 'mdp' => $this->_hash_psw($psw)])->first_row();
            //On test si elle est vide ou non.
            if ($query)
            {
                return $query;
            }
            else
            {
                return array();
            }
        }

        public function activate_account(string $id_comp)
        {
            $this->db->query("UPDATE entreprise SET activated = 1 WHERE id_user = '$id_comp';");

            $res = $this->db->query("SELECT * FROM users U INNER JOIN entreprise E ON U.id_user = E.id_user WHERE E.id_user ='$id_comp' LIMIT 1;")->row(0);

            $link = anchor("Home", "Click here to create offers !");
            $message = "<html><body>" .
                "Hi !$res->nom<br/>" .
                "Your account has been activated<br/>" .
                "You can now create internship offers on our website :<br/>" .
                "$link<br/>" .
                "</body></html>";

            mail($res->mail, "Account activation", $message, "Content-Type: text/html; charset=UTF-8\r\n");

        }
    }
