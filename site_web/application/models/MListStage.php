<?php

class MListStage extends CI_Model {
	function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public static function checkQueryResult($query) {
		//I regroup this redondant part of code.
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return array();
		}
	}

	public function getAll(bool $ordered_by_like)
	{
		//Execute the query
		$query = $this->db->query(
			"select *,
				(SELECT COUNT(L.id_offre) FROM liker L
				INNER JOIN offre O2 ON O2.id_offre = L.id_offre
				WHERE L.id_offre = O.id_offre)
			AS likes from offre O
			INNER JOIN entreprise U on O.id_user = U.id_user
			ORDER BY " . ($ordered_by_like?'likes':'date_crea') . " DESC;"
		);


		//If the result is empty we return an empty array.
		return self::checkQueryResult($query);
	}



	//Permet de récupéré les données sur une offre précisément
	public function getOffersById(int $id) {
		//The same thing than getAll() but with id in where condition.
		$query = $this->db->query(
			"select * from offre O
			join entreprise E on O.id_user = E.id_user
			where O.id_offre = $id;"
		);

		return self::checkQueryResult($query);
	}

	//Est chargé d'insérer l'offre dans la BDD
	public function insertNewOffer(&$input) : bool {
		//On véridie les données
		if ($this->checkData2Insert($input)) {
			//On met en forme les données
			$donnees = self::data2array($input);
			//On fait l'insertion.
			$this->db->insert('offre', $donnees);
			return true;
		} else {
			return false;
		}
	}

	//Est chargé d'update la base de donnée
	public function updateOffer(&$input) : bool {
		//On vérifie les données
		if ($this->checkData2Insert($input)) {
			//On met en forme les données
			$donnees = self::data2array($input);
			//On fait l'update
			$this->db->where('id_offre', $input['id_offre']);
			$this->db->update('offre', $donnees);
			return true;
		} else {
			return false;
		}
	}

	//Vérifie que les données a insérer sont bonnes.
	private function checkData2Insert(&$data) {
		//Les variables obligatoires sont toutes défini
		if (
			isset($data['id_user']) && $data['id_user'] == '' &&
			isset($data['titre']) && $data['titre'] == '' &&
			isset($data['date_deb']) && $data['date_deb'] == '' &&
			isset($data['date_fin']) && $data['date_fin'] == '' &&
			isset($data['contact']) && $data['contact'] == '' &&
			isset($data['mission']) && $data['mission'] == ''
		) {
			$data['err'][] = "Une/Des donnée(s) obligatoire(s) est/sont manquante(s).";
		}

		//Les variable non obligatoires qui sont vide sont mise à NULL.
		$data['commentaire'] ?? 'NULL';

		//On va maintenant vérifier les dates.
		$format = 'Y-m-d';
		$d[0] = DateTime::createFromFormat($format, $data['date_deb']);
		$d[1] = DateTime::createFromFormat($format, $data['date_fin']);

		if (!(
			$d[0] &&
			$d[1] &&
			($d[0]->format($format) == $data['date_deb']) &&
			($d[1]->format($format) == $data['date_fin']) &&
			($d[0]->diff($d[1])->invert < 1)
		)) {
			$data['err'][] = "Dates invalides, vérifiez le format ou que la fin du stage ne précède pas le début.";
		}

		//On retourne ce qu'il faut
		return (isset($data['err']) ? false : true);
	}

	private static function data2array($data) {
		return array(
			'id_user' => $data['id_user'],
			'titre' => $data['titre'],
			'date_fin' => $data['date_fin'],
			'date_deb' => $data['date_deb'],
			'contact' => $data['contact'],
			'mission' => $data['mission'],
			'commentaire' => $data['commentairec']
		);
	}

	public function deleteOfferById($id) {
		$this->db->where('id_offre', $id);
		$this->db->delete('liker');
		$this->db->delete('offre');
	}

	public function already_liked($id_user, $id_offre):bool
	{
		$query = $this->db->query("SELECT id_user FROM liker L
		WHERE id_user = $id_user AND id_offre = $id_offre");
		return ($query->row(0) != NULL);
	}

	public function unlike($id_user, $id_offre)
	{
		$query = $this->db->query("DELETE FROM liker WHERE id_user = $id_user AND id_offre = $id_offre");
	}

	public function like($id_user, $id_offre)
	{
		$query = $this->db->query("INSERT INTO liker VALUES ($id_user,$id_offre);");
	}




}
