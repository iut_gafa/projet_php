<?php
class MUsers extends CI_Model {
	public function __construct() {
		parent::__construct();

		$this->load->database();
	}

	public static function checkQueryResult($query) {
		//I regroup this redondant part of code.
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return array();
		}
	}

	public function listUsers(string $userType) {
		$this->db->join('users', $userType.'.id_user = users.id_user');
		$query = $this->db->get($userType);

		return self::checkQueryResult($query);
	}

	public function userById(string $userType, int $id) {
		$this->db->join('users', $userType.'.id_user = users.id_user');
		$this->db->where('users.id_user = '.$id);
		$query = $this->db->get($userType);

		return self::checkQueryResult($query);
	}

	public function deleteUser(string $userType, int $id) {
		$this->db->where('id_user', $id);
		$this->db->delete('liker');
		$this->db->where('id_user', $id);
		$this->db->delete($userType);
		$this->db->where('id_user', $id);
		$this->db->delete('users');
	}
}
