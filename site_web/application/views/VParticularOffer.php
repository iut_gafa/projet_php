<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title> <?php echo $stageInfo[0]->titre; ?> </title>
	</head>

	<body>
		<?php foreach ($stageInfo as $row) : ?>
			<h1> <?php echo $row->titre; ?> </h1>
			<?php $index=array("raison_sociale", "date_deb", "date_fin", "mission", "contact", "commentaire", "piece_jointe");
			foreach ($index as $currentIndex) :?>
				<p> <?php echo $currentIndex?> : <?php echo $row->$currentIndex??'' ?> </p>
			<?php endforeach; ?>

			<?php if ($userGotRights) : ?>
				<div class="edit">
					<?php echo anchor("/Entreprise/editOffer/$row->id_offre", 'Editer') ?>
				</div>
				<div class="delete">
					<?php echo anchor("/Entreprise/deleteOffer/$row->id_offre", 'Supprimer') ?>
				</div>
			<?php endif;?>
		<?php endforeach; ?>
	</body>
</html>
