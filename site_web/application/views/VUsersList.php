<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title> List of all <?php echo $userType ?> </title>
	</head>
	<body>
		<table>
			<tr>
				<th> ID </th>
				<th><?php echo $userType == 'entreprise' ? 'Corporate Name' : 'Date de naissance' ?></th>
				<th><?php echo $userType == 'entreprise' ? 'Dirigeant first name' : 'First name' ?></th>
				<th> Last name </th>
			</tr>
			<?php foreach ($listUsers as $user) : ?>
				<tr>
					<td><?php echo anchor("/Admin/userDetails/$userType/$user->id_user", $user->id_user); ?></td>
					<td><?php $particularity = $userType == 'membre' ? 'date_nais' : 'raison_sociale';
						echo $user->$particularity?></td>
				<?php if($userType == 'membre') : ?>
					<td><?php echo $user->prenom; ?></td>
					<td><?php echo $user->nom; ?></td>
				<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</table>
	</body>
</html>
