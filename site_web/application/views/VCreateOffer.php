<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title> <?php echo (isset($formInput['id_offre']) ? "Modification" : "Création") ?> d'une offre de stage </title>
	</head>

	<body>
		<div class="sectionTitle">
			<h1> <?php echo (isset($formInput['id_offre']) ? "Modification" : "Création") ?> Offre de stage</h1>
		</div>

		<?php if (isset($formInput['err'])) : ?>
			<div class="warning">
				<?php foreach ($formInput['err'] as $err) : ?>
					<p> <?php echo $err; ?> </p>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>

		<form class="createOffer" action="<?php echo base_url() ?>Entreprise/validateStockIntoDB/" method="post">
			<!-- id_offre -->
			<input type="hidden" name="id_offre" value="<?php echo $formInput['id_offre'] ?? '' ?>">
			<input type="hidden" name="id_user" value="<?php echo $formInput['id_user'] ?? '' ?>">
			<!-- Titre -->
			<div class="inputElement">
				<label for="titre"> titre : </label>
				<input type="text" name="titre" placeholder="titre du stage" maxlength="50" size="50" value="<?php echo ($formInput['titre'] ?? '') ?>" required>
			</div>

			<!-- Date de début -->
			<div class="inputElement">
				<label for="date_deb" required> Date de début : </label>
				<input value="<?php echo ($formInput['date_deb'] ?? '') ?>" type="date" name="date_deb" required>
			</div>

			<!-- Durée -->
			<div class="inputElement">
				<label for="date_fin"> Date de fin </label>
				<input value="<?php echo ($formInput['date_fin'] ?? '') ?>" type="date" name="date_fin" required>
			</div>

			<!-- Contact -->
			<div class="inputElement">
				<label for="contact"> Contact </label>
				<input value="<?php echo ($formInput['contact'] ?? '') ?>" type="text" name="contact" placeholder="email/numéro/BP" size="100" required>
			</div>

			<!-- description de la mission -->
			<div class="inputElement">
				<label for="mission"> Description de l'offre : </label> </br>
				<textarea required name="mission" rows="10" cols="80" placeholder="Description"><?php echo ($formInput['mission'] ?? '') ?></textarea>
			</div>

			<!-- Commentaire -->
			<div class="inputElement">
				<label for="commentairec"> Commentaire : </label> <br>
				<textarea name="commentairec" rows="10" cols="80" placeholder="Commentaire sur l'offre"><?php echo ($formInput['commentaire'] ?? '') ?></textarea>
			</div>

			<!-- Boutons -->
			<div class="inputElement">
				<button type="submit" name="valider" value="valider"> Valider </button>
				<button type="submit" name="button" value="annuler"> Annuler </button>
			</div>
		</form>
	</body>
</html>
