<?php $this->load->library('session'); ?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title> liste des Stages </title>
		<script src="https://kit.fontawesome.com/e173c96920.js" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

		<style>
			.big {
				min-width: 25%;
			}
		</style>
	</head>
	<body>
		<div class="d-flex dropdown justify-content-center">
		  	<button class="btn btn-info dropdown-toggle big" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		    	Sorted by <?= ($sort_like?'Likes':'Dates') ?>
		  	</button>
		  	<div class="dropdown-menu text-center big" aria-labelledby="dropdownMenuButton">
		    	<a class="dropdown-item" <?= ($sort_like?'':'hidden') ?> href="<?= base_url();?>Home/index/date">Dates</a>
		    	<a class="dropdown-item" <?= ($sort_like?'hidden':'') ?> href="<?= base_url();?>Home/index/like">Likes</a>
		  	</div>
		</div>

		<div class="list">
			<div class="list-group">
				<li class="list-group-item list-group-item-dark">
					<div class="d-flex w-100 justify-content-between">
						<h5 class="mb-1">Internship Title</h5>
						<small>Published date</small>
					</div>
					<p class="mb-1">Company</p>
					<small>Description of the Internship.</small>
				</li>
				<?php foreach ($listStage as $index => $stage) : ?>
					<li class="list-group-item list-group-item-<?= $index%2?'light':'info'?>">
						<a href="<?= base_url();?>Offers/offersDetails/<?= $stage->id_offre ?>" class="list-group-item list-group-item-action ">
							<div class="d-flex w-100 justify-content-between">
								<h5 class="mb-1"><?= $stage->titre ?></h5>
								<small><?= date_diff(date_create($stage->date_crea), new DateTime("now"))->format('%a');  ?> days ago</small>
							</div>
							<p class="mb-1"><?= $stage->raison_sociale ?></p>
							<small><?= $stage->mission ?></small>
					    </a>
						<form>
							<button
								<?= isset($this->session->user_info)?"formaction=\"" . base_url() . "Home/like/$stage->id_offre\" ":''; ?>
								type="submit" class="btn badge badge-pill badge-danger">
									<?= $stage->likes ?>
								<i class="far fa-heart">
								</i>
							</button>
						</form>
					</li>
				<?php endforeach; ?>
			</div>
		</div>
	</body>
</html>
