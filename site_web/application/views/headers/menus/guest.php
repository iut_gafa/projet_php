<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="<?= base_url();?>">Intership Offers</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<?= base_url();?>">All Offers<span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <form class="form-inline">
            <?php $this->load->view('headers/signin_b'); ?>
            <?php $this->load->view('headers/signup_b'); ?>
        </form>
    </div>
</nav>
<?php $this->load->view('headers/signin_m'); ?>
<?php $this->load->view('headers/signup_m'); ?>
