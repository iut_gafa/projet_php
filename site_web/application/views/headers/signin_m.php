<?php $this->load->helper(array('form', 'url')); $this->load->library('session');?>

<?= $this->session->flashdata('modal_settings')??'' ?>
<!-- Modal -->
<div class="modal fade" id="signinModal" tabindex="-1" role="dialog" aria-labelledby="signinModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sign In</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo validation_errors(); ?>
            <form action="<?= base_url();?>Log/trylog/<?= ($this->session->flashdata('page') == '')?'true/welcome_message':$this->session->flashdata('page'); ?>" method="post" name="login">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="log_email">Email address</label>
                        <input type="email" name="log_email" value="<?= set_value('log_email')??'' ?>" class="form-control <?= $this->session->flashdata('is_mail_valid_si')??'' ?>" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                        <div class="valid-feedback">Looks good!</div>
                        <div class="invalid-feedback">We do not know this email.</div>
                        <small id="emailHelp" <?= ($this->session->flashdata('is_mail_valid_si') != '')?'hidden':''; ?> class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="psw">Password</label>
                        <input type="password" name="psw" value="<?= set_value('psw')??'' ?>" class="form-control <?= $this->session->flashdata('is_psw_valid_si')??'' ?>" id="exampleInputPassword1" placeholder="Password">
                        <div class="valid-feedback">Looks good!</div>
                        <div class="invalid-feedback">This password does not match.</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="action" value="SignIn" class="btn btn-info">LogIn</button>
                </div>
            </form>
        </div>
    </div>
</div>
