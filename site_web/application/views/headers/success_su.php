<?php $this->load->helper(array('form', 'url')); ?>


<!-- Modal -->
<div class="modal fade" id="successModal_su" tabindex="-1" role="dialog" aria-labelledby="successModal_su" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Success</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>You have successfully registered !</p>
                <p <?= $success_company??'hidden'; ?>>
                    As a company you can create offers, but you must wait that the administrator enable your account.
                    <br/>
                    However, you can still use your account as a normal user.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
