<?php
    $this->load->library('session');
    if (isset($this->session->user_info))
    {
        if ($this->session->user_info->statut == "membre")
        {
            $this->load->view('headers/menus/member');
        }
        else if ($this->session->user_info->statut == "entreprise")
        {
            $this->load->view('headers/menus/company');
        }
        else
        {
            $this->load->view('headers/menus/admin');
        }
    }
    else
    {
        $this->load->view('headers/menus/guest');
    }
?>
