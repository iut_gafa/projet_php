<?php $this->load->helper(array('form', 'url')); $this->load->library('session');?>
<style>
    .person-field, .corp-field
    {
        display: none;
    }

    .person-field.active, .corp-field.active
    {
        display: block;
    }
</style>

<?= $this->session->flashdata('modal_settings')??'' ?>

<!-- Modal -->
<div class="modal fade" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="signupModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo validation_errors(); ?>
            <form action="<?= base_url();?>Log/signup/<?= ($this->session->flashdata('page') == '')?'true/welcome_message':$this->session->flashdata('page'); ?>" method="post" name="signup">
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="log_email">Email address</label>
                                <input type="email" name="sup_email" value="<?= set_value('sup_email')??'' ?>" class="form-control <?= $this->session->flashdata('is_email_valid_su')??'' ?>" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
                                <div class="valid-feedback">You can use this email!</div>
                                <div class="invalid-feedback">This email is already taken.</div>
                                <small id="emailHelp" class="form-text text-muted" <?= ($this->session->flashdata('is_email_valid_su') != '')?'hidden':''; ?>>We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group">
                                <label for="sup_pswa">Password</label>
                                <input type="password" name="sup_pswa" value="<?= set_value('sup_pswa')??'' ?>" class="form-control <?= $this->session->flashdata('is_psw_valid_su')??'' ?>" id="exampleInputPassword1" placeholder="Enter Password" required>
                                <div class="valid-feedback">Passwords match!</div>
                                <div class="invalid-feedback">Passwords don't match.</div>
                            </div>
                            <div class="form-group custom-control custom-switch">
                                <input type="checkbox" <?= isset($_POST['is_company'])?'checked="checked"':'' ?> name="is_company" class="custom-control-input" id="is-company" autocomplete="off">
                                <label class="custom-control-label" for="is-company">I'm a company.</label>
                            </div>
                            <div class="form-group corp-field <?= isset($_POST['is_company'])?'active':'' ?>">
                                <label for="soc_reas">Social Reason</label>
                                <input type="text" name="soc_reas" value="<?= set_value('soc_reas')??'' ?>" class="form-control corp-input" placeholder="Enter Social Reason">
                            </div>
                            <div class="form-group person-field <?= isset($_POST['is_company'])?'':'active' ?>">
                                <label for="first_name">First Name</label>
                                <input type="text" name="first_name" value="<?= set_value('first_name')??'' ?>" class="form-control person-input" placeholder="Enter First Name">
                                <label for="birth_day">Birth Day</label>
                                <input type="date" name="birth_day" value="<?= set_value('birth_day')??'' ?>" class="form-control person-input" >
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="inputTel">Phone Number</label>
                                <input type="tel" placeholder="ex : 0612345678" name="tel" class="form-control <?= $this->session->flashdata('is_tel_valid_su')??'' ?>" id="inputTel" value="<?= set_value('tel')??'' ?>" required>
                                <div class="valid-feedback">This phone number is ok!</div>
                                <div class="invalid-feedback">This is not a phone number.</div>
                                <small id="" class="form-text text-muted d-none d-lg-block"><?= ($this->session->flashdata('is_tel_valid_su') != '')?'':'&nbsp;' ?></small>
                            </div>
                            <div class="form-group">
                                <label for="sup_pswb">Confirm Password</label>
                                <input type="password" name="sup_pswb" value="<?= set_value('sup_pswb')??'' ?>" class="form-control <?= $this->session->flashdata('is_psw_valid_su')??'' ?>" id="exampleInputPassword1" placeholder="Enter Password" required>
                                <div class="valid-feedback">Passwords match!</div>
                                <div class="invalid-feedback">Passwords don't match.</div>
                            </div>
                            <div class="form-group d-none d-lg-block">
                                &nbsp;
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="sup_name" value="<?= set_value('sup_name')??'' ?>" class="form-control" placeholder="Enter Name" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="action" value="SignUp" class="btn btn-info">Sign Up</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('#is-company').on('change', function()
        {
            $('.corp-field').toggleClass('active');
            $('.person-field').toggleClass('active');

            if ($('#is-company').is(':checked'))
            {
                $('.corp-input').prop('required', true);
                $('.person-input').removeAttr('required');
            }
            else
            {
                $('.person-input').prop('required', true);
                $('.corp-input').removeAttr('required');
            }
        }
    );
</script>
