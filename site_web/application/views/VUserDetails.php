<?php
if($userType == 'membre'){
	$title = ($userData[0]->nom.' '.$userData[0]->prenom);
} else {
	$title = $userData[0]->raison_sociale;
}

$index = array('id_user', 'mail', 'tel');
if ($userType == 'membre') {
	$index = array_merge($index, array('nom', 'prenom'));
}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php echo $title ?></title>
	</head>
	<body>
		<h1><?php echo $title ?></h1>
		<?php foreach($index as $currentIndex) : ?>
			<p><?php echo $currentIndex; ?> : <?php echo $userData[0]->$currentIndex;?></p>
		<?php endforeach; ?>

		<div class="edit">
			<?php echo anchor("/Admin/editUser/".($userData[0]->id_user).'/'.($userData[0]->statut), 'Editer'); ?>
		</div>
		<div class="delete">
			<?php echo anchor("/Admin/deleteUser/".($userData[0]->id_user).'/'.($userData[0]->statut), 'Supprimer'); ?>
		</div>

		</div>
	</body>
</html>
