-- INSERTION ADMIN :
INSERT INTO users VALUES (DEFAULT, 'Luitot', 'gaetan.luitot@etu.univ-amu.fr', '2ac57d6e9877a8ba7879a89f3643f5bc47e48a93', '0677241623', 'membre');
INSERT INTO membre VALUES ((SELECT id_user FROM users WHERE mail = 'luitot@hotmail.fr' LIMIT 1), 'Gaëtan', '2000-10-24');

-- INSERTION DES UTILISATEURS :
INSERT INTO users VALUES (DEFAULT, 'Wizards of the cost', 'press@wizards.com', 'a2a32cb17a4ce05bbbcea917cc435bf7fa591c56', '0894239039', 'entreprise');
INSERT INTO entreprise VALUES ((SELECT id_user FROM users WHERE mail = 'press@wizards.com' LIMIT 1), 'Wizards of the cost', 0);

INSERT INTO users VALUES (DEFAULT, 'Nintendo', 'mail@nintendo.com', '5c5afe46d96c4e3db441dfb0de006af6aba90b4e', '0771164516', 'entreprise');
INSERT INTO entreprise VALUES ((SELECT id_user FROM users WHERE mail = 'mail@nintendo.com' LIMIT 1), 'Nintendo', 0);

INSERT INTO users VALUES (DEFAULT, 'Amplitude Studio', 'amplitude@studio.com', 'b9e402b83b56af69ccfee94183784f3f18c32057', '0715408098', 'entreprise');
INSERT INTO entreprise VALUES ((SELECT id_user FROM users WHERE mail = 'amplitude@studio.com' LIMIT 1), 'Amplitude Studio', 0);

-- INSERTION DES OFFRES :

INSERT INTO offre VALUES (DEFAULT, (SELECT id_user FROM users WHERE nom = 'Wizards of the cost' LIMIT 1), 'Software Developer - Rules Engine', '2019-03-31', '2019-03-31', '2019-11-24 00:43:07', 'NULL', 'press@wizards.com', 'We implement game mechanics and game-flow features for Magic: the Gathering Arena. In this role you’ll collaborate frequently with game designers and front-end developers to deliver a snappy and engaging digital experience, but with all the strategy of authentic Magic.', 'NULL');
INSERT INTO offre VALUES (DEFAULT, (SELECT id_user FROM users WHERE nom = 'Wizards of the cost' LIMIT 1), 'Marketing Technology Manager', '2012-03-31', '2021-03-31', '2019-10-24 00:43:07', 'NULL', 'press@wizards.com', 'Greetings, brave adventurer! We\'re looking for a marketing technology wizard to join the ranks of our marketing guild and help build the future of the realm. Ready to slay technology dragons? Venture forth!', 'NULL');

INSERT INTO offre VALUES (DEFAULT, (SELECT id_user FROM users WHERE nom = 'Nintendo' LIMIT 1), 'Marketing Technology Manager', '2015-03-31', '2021-03-31', '2019-11-10 00:43:07', 'NULL', 'mail@nintendo.com', 'A developing individual contributor that assists in developing, writing, and debugging of code for assigned software projects.
    Implement defined subset of tasks of assigned team and project.
    Assists in developing, writing, testing, debugging and implementing code using the relevant programming languages.
    Participates in product testing and maintenance activities as required by project management.
    May assist in tool development with other engineers.
    Applies professional expertise to review, analyze and test products under development as a contributing member of a production team to ensure delivery of Nintendo’s high standard of quality and timeliness.', 'NULL');

INSERT INTO offre VALUES (DEFAULT, (SELECT id_user FROM users WHERE nom = 'Amplitude Studio' LIMIT 1), 'Full Stack Developer ', '2017-03-14', '2021-11-08', '2018-11-10 00:43:07', 'NULL', 'amplitude@studio.com',
'We are looking for a Web Developer to assist our Technical Lead in the conception and implementation of our Games2Gether platform. Specifically, we are seeking an autonomous candidate with strong technical skills and the ambition to become a full-stack developer.', 'NULL');
