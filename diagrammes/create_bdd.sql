-- Création du schéma de donnée
create schema offres_stages;

-- Création d'un type statut
create domain DOM_statut as varchar
	constraint statut_val
		check (value similar to 'admin' or 'user' or 'entreprise')

-- Table User.
create table user (
	id_user int autoincrement not null,
	prenom varchar,
	mail varchar,
	mdp varchar,
	statut varchar
)
